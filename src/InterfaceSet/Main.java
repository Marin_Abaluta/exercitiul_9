package InterfaceSet;

import java.lang.reflect.Array;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        HashSet<String> Set1 = new HashSet<String>();

        Set1.add("Samuel Smith");
        Set1.add("Bill Williams");
        Set1.add("Anthony Johnson");
        Set1.add("Cartner Caragher");

        System.out.println("myEmployeesSet1: " + Set1);

        List lista = new ArrayList();

        lista.add("Astrid Conner");
        lista.add("Christopher Adams");
        lista.add("Antoine Griezmann");
        lista.add("Adam Sandler");
        lista.add("Bailey Aidan");
        lista.add("Carl Edwin");
        lista.add("Carl Edwin");

        HashSet<String> Set2 = new HashSet<String>(lista);

        System.out.println("list: " + lista);
        System.out.println("myEmployeesSet2: " + Set2);


        boolean match = (Set1.equals(Set2));
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + match);

        Set2.remove("Carl Edwin");
        System.out.println("Removed Carl Edwin from myEmployeeSet2");

        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + match);

        System.out.println("myEmployeesSet1 contains all the elements: " + Set1.containsAll(lista));
        System.out.println("myEmployeesSet2 contains all the elements: " + Set2.containsAll(lista));

        Iterator it = Set1.iterator();

        while (it.hasNext()){

            String employee = it.next().toString();
            System.out.println("Iterator loop: " + employee);

        }

        Set1.clear();

        System.out.println("myEmployeesSet1 is Empty: " + Set1.isEmpty());
        System.out.println("myEmployeesSet1 has: " + Set1.size() + " Elements");
        System.out.println("myEmployeesSet1 has: " + Set2.size() + " Elements");

        String[] array = new String[Set2.size()];
        Set2.toArray(array);

        //for(String str:array){System.out.println(str);}

        int m = 0;
        for (String s: Set2)
            array[m++] = s;

        System.out.println("The array: " + Arrays.toString(array));


    }
}
